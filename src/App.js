import React, { Component } from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import Login from './components/login';
import Register from './components/register';
import Home from './components/home';
import Header from './components/header';
class App extends Component { 
  render() {
    return (
      <div className="App">
        <Header/>
        <BrowserRouter>
            <React.Fragment>  
                <Route path="/" component={Login}/>              
                <Route path="/register" component={Register}/>
                <Route path="/home" component={Home}/>
            </React.Fragment>
        </BrowserRouter>       
      </div>
    );
  }
}

export default App;
