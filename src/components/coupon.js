import React, { Component } from 'react';
import {Container,Row,Col,Button,FormGroup,FormControl} from 'react-bootstrap';
class Cupon extends Component {
  constructor(props){
    super(props);
    this.state={
      email:"",
      password:""
    };
    
  }
  handleChange = event =>{
    this.setState({
      [event.target.id]: event.target.value
    });
  }
  handleSubmitCupon = event =>{
    event.preventDefault();
    const form = {
      code: this.state.code
    }
    fetch('https://frontend-test.serempre.com/api/redeem/',{
      method:'POST',
      headers:{
        'Authorization':'Bearer '+localStorage.getItem("token"),
        'Content-type':'application/json',
      },
      body:JSON.stringify({
        "code": form.code
      })
    }).then((response) =>{
      console.log(response.status);
      if(response.status === 400){
        response.json().then((data) => {
            for(var i in data){
                alert(i+" "+data[i]);
            }
        });
      }else{
        response.json().then((data) => {
            alert("El cupon a sido redimido con éxito");
        });
      }
      
    })
  }
  render() {
    return (
      <div>
          <Container>
              <Row>
                  <Col md={12}>
                    <br/><h2>Redimir Cupon</h2><br/>
                  </Col>
                  <Col md={12} onSubmit={this.handleSubmitCupon}>
                    <form>
                        <FormGroup controlId="code" >
                        <label>Codigo</label>
                        <FormControl
                          autoFocus
                          type="text"
                          value={this.state.code}
                          onChange={this.handleChange}
                        />
                      </FormGroup>
                      <Button
                        block
                        
                        
                        type="submit"
                      >
                        Redimir
                      </Button>
                      
                    </form>
                  </Col>
              </Row>
          </Container>
      </div>
    );
  }
}

export default Cupon;