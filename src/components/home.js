import React, { Component } from 'react';
import {Container,Row,Col,Button,FormGroup,FormControl} from 'react-bootstrap';
import Cupon from './coupon';
class Home extends Component {
  constructor(props){
    super(props);
  }
  
  getuserdata(){
    let htmlUser = "";
    fetch('https://frontend-test.serempre.com/api/profile/',{
      method:'GET',
      headers:{
        'Authorization':'Bearer '+localStorage.getItem("token"),
        'Content-type':'application/json',
      }
    }).then((response) =>{
      console.log(response.status);
      if(response.status=== 401){
        response.json().then((data) => {
            for(var i in data){
              alert(i+" "+data[i]);
            }
            localStorage.setItem("token", "");
            window.location = "/";
        });
      }else{
        response.json().then((data) => {
          
          for(var i in data){
            console.log(i+" "+data[i]);
            htmlUser += "<p><b> "+i+"</b> "+data[i]+"</p>";
          }
          document.getElementById("userData").innerHTML=htmlUser;
        });
      }
      
    });
  }
  
  render() {
    return (
      <Container>
        <Row>
          <Col md={6}>
            <h1>Bienvenido</h1>
            {this.getuserdata()}
            <div id="userData"></div>
          </Col>
          <Col md={6}>
          <Cupon></Cupon>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Home;