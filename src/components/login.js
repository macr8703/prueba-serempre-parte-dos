import React, { Component } from 'react';
import {Container,Row,Col,Button,FormGroup,FormControl} from 'react-bootstrap';
class Login extends Component {
  constructor(props){
    super(props);
    this.state={
      email:"",
      password:""
    };
    
  }
  handleChange = event =>{
    this.setState({
      [event.target.id]: event.target.value
    });
  }
  handleSubmit = event =>{
    event.preventDefault();
    const form = {
      email: this.state.email,
      password: this.state.password
    }

    console.log(form);
    fetch('https://frontend-test.serempre.com/api/auth/',{
      method:'POST',
      headers:{
        'Accept':'application/json',
        'Content-type':'application/json',
      },
      body:JSON.stringify({
        "email": form.email,
        "password":form.password,
      })
    }).then((response) =>{
      if(response.status === 400){
        response.json().then((data) => {
            for(var i in data){
              if(i === "non_field_errors"){
                alert(data[i]);
              }else{
                alert(i+" "+data[i]);
              }
            }
        });
      }else{
        response.json().then((data) => {
            localStorage.setItem("token", data.token);
            setTimeout(function(){
              window.location = '/home';
            },100);
        });
      }
      
    })
  }
  hideLogin(){
    let gurl = window.location.href;
    let surl = gurl.split("/");
    let lasturlp = surl[(surl.length)-1];
    if(lasturlp !=""){
      setTimeout(function(){
        document.getElementById("login-container").style.display = "none";
      },10);
    }
    
    
  }
  render() {
    return (
      <div id="login-container">
      <Container>
        <Row>
          <Col md={12}>
            <h1>Login</h1>
          </Col>
          <Col md={12}>
          <form onSubmit={this.handleSubmit}>
            <FormGroup controlId="email" >
              <label>Email</label>
              <FormControl
                autoFocus
                type="email"
                value={this.state.email}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup controlId="password" >
              <label>Password</label>
              <FormControl
                autoFocus
                type="password"
                value={this.state.password}
                onChange={this.handleChange}
              />
            </FormGroup>
            <Button
              block
              
              
              type="submit"
            >
              Login
            </Button>
            <p>Or <a href="/register">Register here</a></p>
          </form>
          </Col>
        </Row>
      </Container>
      {this.hideLogin()}
      </div>
    );
  }
}

export default Login;