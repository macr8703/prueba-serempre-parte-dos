import React, { Component } from 'react';
import {Container,Row,Col,Button,FormGroup,FormControl} from 'react-bootstrap';
import Form from 'react-bootstrap/FormControl';
class Register extends Component {
  constructor(props){
    super(props);
    this.state={
      email:"",
      password:""
    };
    
  }
  
  handleChange = event =>{
    this.setState({
      [event.target.id]: event.target.value
    });
  }
  handleSubmit = event =>{
    event.preventDefault();
    const form = {
      email: this.state.email,
      password: this.state.password,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      id_number: this.state.id_number,
      id_type: this.state.id_type,
      phone: this.state.phone,
      gender: this.state.gender,
      address: this.state.address,
      city: this.state.city,
      birthday:this.state.birthday
    }
    console.log("form.id_type");
    console.log(form.id_type);
    fetch('https://frontend-test.serempre.com/api/register/',{
      method:'POST',
      headers:{
        'Accept':'application/json',
        'Content-type':'application/json',
      },
      body:JSON.stringify({
        "first_name": form.firstName,
        "last_name": form.lastName,
        "email": form.email,
        "id_type": form.id_type,
        "id_number": form.id_number,
        "phone": form.phone,
        "gender": form.gender,
        "address": form.address,
        "city": form.city,
        "birthday": form.birthday,
        "password": form.password
      })
    }).then((response) =>{
      if(response.status=== 400){
        response.json().then((data) => {
            for(var i in data){
                alert(i+" "+data[i]);
            }
        });
      }else{
        response.json().then((data) => {
            window.location = '/';
        });
      }
    })
  }
  render() {
    return (
      <Container>
        <Row>
          <Col md={12}>
            <h1>Registro</h1>
          </Col>
          <Col md={12}>
          <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="firstName">
              <label>first Name</label>
              <FormControl
                autoFocus
                type="text"
                value={this.state.firstName}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup controlId="lastName" >
              <label>last Name</label>
              <FormControl
                autoFocus
                type="text"
                value={this.state.lastName}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup controlId="id_type" >
              <label>id_type</label>
              <FormControl
                autoFocus
                type="text"
                value={this.state.id_type}
                onChange={this.handleChange}
              />
            </FormGroup>
            
            <FormGroup controlId="id_number" >
              <label>id_number</label>
              <FormControl
                autoFocus
                type="text"
                value={this.state.id_number}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup controlId="gender" >
              <label>gender</label>
              <FormControl
                autoFocus
                type="text"
                value={this.state.gender}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup controlId="email" >
              <label>Email</label>
              <FormControl
                autoFocus
                type="email"
                value={this.state.email}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup controlId="address" >
              <label>address</label>
              <FormControl
                autoFocus
                type="text"
                value={this.state.address}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup controlId="city" >
              <label>city</label>
              <FormControl
                autoFocus
                type="text"
                value={this.state.city}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup controlId="birthday" >
              <label>birthday</label>
              <FormControl
                autoFocus
                type="text"
                value={this.state.birthday}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup controlId="phone" >
              <label>Phone</label>
              <FormControl
                autoFocus
                type="text"
                value={this.state.phone}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup controlId="password" >
              <label>Password</label>
              <FormControl
                autoFocus
                type="password"
                value={this.state.password}
                onChange={this.handleChange}
              />
            </FormGroup>
            <Button
              block
              
              
              type="submit"
            >
              Registrar
            </Button>
          </form>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Register;